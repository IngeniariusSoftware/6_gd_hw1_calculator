package CalculatorGD;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Calculator calculator = new Calculator();
        System.out.println("Добро пожаловать в калькулятор!");
        String cmd;
        Scanner scanner = new Scanner(System.in);
        do {
            calculator.getInfo();
            System.out.println("напишите exit для выхода, нажмите enter, чтобы продолжить вычисления");
            cmd = scanner.nextLine();
        } while (!cmd.equals("exit"));
    }
}
