package CalculatorGD;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

import static java.lang.Double.parseDouble;

public class Calculator {

    private HashSet<String> operationSet = new HashSet<String>(Arrays.asList("+", "-", "*", "/", "sin", "%" , "sqrt", "pow", "cos", "log", "tan", "!"));
    
    private String sign;

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) { this.sign = sign; }

    double x, y, result;

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Calculator() {

    }

    public void getInfo() {
        System.out.print("Знак: ");
        Scanner scanner = new Scanner(System.in);
        String sign = scanner.nextLine();
        setSign(sign);
        boolean passed = false;
        if (checkSign()) {

            do {
                System.out.println("X = ");
                try {
                    setX(parseDouble(scanner.next()));
                    passed = true;
                } catch (NumberFormatException ex) {
                    System.out.println("Ошибка ввода, повторите ввод");
                }
            } while (!passed);

            passed = false;
            if (!(getSign().equals("sin") || getSign().equals("cos") || getSign().equals("tan") || getSign().equals("!"))){
                do {
                    System.out.println("Y = ");
                    try {
                        setY(parseDouble(scanner.next()));
                        passed = true;
                    } catch (NumberFormatException ex) {
                        System.out.println("Ошибка ввода, повторите ввод");
                    }
                } while (!passed);
            }
            makeOperations();
        } else {
            if (getSign().equals("0")) {
                System.exit(0);
            } else {
                System.out.println("Неверно указан знак операции");

            }
        }
    }

    public void makeOperations() {
        switch (sign) {
            case "+":
                summarize();
                break;
            case "-":
                subtract();
                break;
            case "*":
                multiply();
                break;
            case "/":
                divide();
                break;
            case "sqrt":
                sqrt();
                break;
            case "sin":
                sinus();
                break;
            case "cos":
                cosine();
                break;
            case "%":
                rest();
                break;
            case "pow":
                pow();
                break;
            case "log":
                logarithm();
                break;
            case "tan":
                tangent();
                break;
            case "!":
                factorial();
                break;
        }
    }

    void summarize() {
        result = x + y;
        printResult();
    }

    void multiply() {
        result = x * y;
        printResult();
    }

    void subtract() {
        result = x - y;
        printResult();
    }

    void divide() {
        if (y != 0) {
            result = x / y;
            printResult();
        } else
            System.out.println("Ошибка - деление на ноль!");
    }

    void sqrt(){
        if(x >= 0){
            if(y != 0){
                result = Math.pow(x, 1.0/y);
                printResult();
            }
            else
                System.out.println("Ошибка - невозможно извлечь такой корень!");
        } else
            System.out.println("Ошибка - корень из отрицательного числа!");
    }

    void sinus(){
        result = Math.sin(x);
        printResult();
    }

    void cosine(){
        result = Math.cos(x);
        printResult();
    }

    void rest() {
        if (y != 0) {
            result = x % y;
            printResult();
        } else
            System.out.println("Ошибка - деление с остатком на ноль!");
    }

    void pow(){
        result = Math.pow(x,y);
        printResult();
    }

    void logarithm(){
        if(y > 0){
            if (x > 0 && x != 1){
                result = Math.log(y) / Math.log(x);
                printResult();
            } else {
                System.out.println("Ошибка - основание логарифма равно 1 или не больше 0!");
            }
        } else {
            System.out.println("Ошибка - логарифм от отрицательного числа!");
        }
    }
    void tangent()
    {
        result = Math.tan(x);
        printResult();
    }

    void factorial()
    {
        if((x < 0) || (x % 1 != 0))
            System.out.println("Ошибка - введенное число не целое положительное!");

        else
        {
            result = 1;
            for(int i = 1; i <= x; i++)
                result = result * i;
            printResult();
        }

    }

    void printResult() {
        System.out.println(result);
    }

    boolean checkSign() {
        return (operationSet.contains(sign));
    }
}
